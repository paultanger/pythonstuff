# script to iterate MSTMap with various parameters and capture summary output
# Paul Tanger

import os
import tempfile
import shutil
import subprocess
import time
from optparse import OptionParser

def main():
    # define mstmap program location
    # this is setup for the BSPM server..
    mstmap = "/usr/local/bin/mstmap"

    #  define some defaults to check
    poptypes = ("kosambi", "haldane")
    objfuns = ("COUNT", "ML")
    # prints out a nice help msg
    usage = "usage: %prog -i <filename> --poptype <DH or RILn> --popname <name> [options] ... will output summary file to current dir.. "
    parser = OptionParser(usage)
    # add options and where they go, and nice help description
    # defaults are in parathesis
    parser.add_option("-i", dest="inputfile",
                      help="genotype filename (required)")
    parser.add_option("--poptype", dest="poptype",
                      help="population type: DH or RILn where n is generation number (required)")
    parser.add_option("--popname", dest="popname",
                      help="name of population (required)")
    parser.add_option("--distfun", dest="distfun", default="kosambi",
                      help="distance function: kosambi, haldane (kosambi)")
    parser.add_option("--cutoffS", dest="cutoffstart", type="float", default=.000001,
                      help="p value cutoff to start iterations, must be <= cutoffE (.000001)")
    parser.add_option("--step", dest="step", type="float", default=0.25,
                      help="iterate between start and end p values by this percentage.  "
                           "For example, the default is 4 iterations run at 0.00000100, 0.00000325, 0.00000550, 0.00000775, 0.00001000")
    parser.add_option("--cutoffE", dest="cutoffend", type="float", default=.000001,
                      help="p value cutoff to end with, must be <= cutoffE (.000001)")
    parser.add_option("--nomapdist", dest="nomapdist", type="float", default=15.0,
                      help="separate markers > this distance away into separate groups (15 cM)")
    parser.add_option("--nomapsize", dest="nomapsize", type="int", default=2,
                      help="groups smaller than this size & further than nomapdist are isolated into their own groups (2)")
    parser.add_option("--miss", dest="miss", type="float", default=0.25,
                      help="percent missing to exclude markers (0.25)")
    parser.add_option("--est", dest="est", default="no",
                      help="estimate missing data.. could take a while.. (no)")
    parser.add_option("--detectbad", dest="detectbad", default="yes",
                      help="detect bad markers (yes)")
    parser.add_option("--objfun", dest="objfun", default="COUNT",
                      help="algorithm: COUNT or ML (COUNT)")
    parser.add_option("-v", "--verbose", default=True,
                      action="store_true", dest="verbose")
    # get arguments
    (options, args) = parser.parse_args()
    
    # check stuff
    if not options.inputfile:
        parser.error("input file not entered")
    if os.path.exists(options.inputfile):
        pass
    else:
        parser.error("input file not found")
    if not options.poptype:
        parser.error("population type not entered")
    if not options.popname:
        parser.error("population name not entered")
    if options.distfun not in poptypes:
        parser.error("incorrect distance function option")
    #if not options.poptype.startswith("DH"):
    #    parser.error("incorrect population type") 
    if (options.cutoffstart > options.cutoffend) or \
            (options.cutoffstart > 1.0) or (options.cutoffend > 1.0) or (options.step > 1.0):
        parser.error("cutoff values incorrect")
    if (options.nomapsize < 0) or (options.nomapdist < 0):
        parser.error("map dist or map size less than zero")
    if (options.miss < 0) or (options.miss > 1.0):
        parser.error("incorrect missing frequency")
    if options.objfun not in objfuns:
        parser.error("incorrect objective function")
        
    # print verbose stuff
    if options.verbose:
        print "\nreading %s..." % options.inputfile
        print "population type is %s" % options.poptype
        print "population name is %s" % options.popname
        print "distance function is %s" % options.distfun
        print "cutoff iterations will start at %.12f and increment by %f of the diff between %.12f and %.12f" \
              % (options.cutoffstart, options.step, options.cutoffstart, options.cutoffend)
        print "groups more than %d and further than %.3f cM away will be segregated into separate groups" \
              % (options.nomapsize, options.nomapdist)
        print "markers with more than %.2f missing will be excluded" % options.miss
        print "estimate missing markers: %s" % options.est
        print "detect bad markers: %s" % options.detectbad
        print "objective function: %s\n" % options.objfun
        
    # get input file and calculate loci & individuals
    # assume it has a header, and start counting lines with line 14
    with open(options.inputfile) as infile:
        # save data for later
        genos = infile.readlines()[12:]
        # get number of loci..subtract 1 for newline at end & 2 for newline & header
        loci = len(genos) - 3
        headerline = genos[1]
        # subtract 1 for marker ID
        indiv = len(headerline.strip().split('\t')) - 1

    # create mstmap input files
    #  try with tempfile library
    # in the default location for that OS..
    tempdir = tempfile.mkdtemp(prefix="mstmap_files")

    # calculate iteration values for p values
    # create list of values to use, and add start value to it
    cutoffiterlist = [options.cutoffstart]
    # calculate steps as % of start
    cutoffstep = abs(options.cutoffend - options.cutoffstart) * options.step
    # skip if no iterations requested..
    i = options.cutoffstart
    if cutoffstep > 0:
        # start at the lowest cutoff
        # continue iterating until cutoff end
        while i < options.cutoffend:
            cutoffiterlist += [i + cutoffstep]
            i +=  cutoffstep
    #print cutoffiterlist

    # generate iterations of headers..
    # outer loops over p values
    for j in cutoffiterlist:
        # todo: inner loops over map size
        mstheader  = ("population_type %s \n"              % options.poptype)
        mstheader += ("population_name %s \n"              % options.popname)
        mstheader += ("distance_function %s \n"            % options.distfun)
        mstheader += ("cut_off_p_value %.12f \n"           % j ) # j is the value of the element in the cutoffiteration list for this iteration
        mstheader += ("no_map_dist %.3f \n"                % options.nomapdist)
        mstheader += ("no_map_size %d \n"                  % options.nomapsize)
        mstheader += ("missing_threshold %.3f \n"          % options.miss)
        mstheader += ("estimation_before_clustering %s \n" % options.est)
        mstheader += ("detect_bad_data %s \n"              % options.detectbad)
        mstheader += ("objective_function %s \n"           % options.objfun)
        mstheader += ("number_of_loci %d \n"               % loci)
        mstheader += ("number_of_individual %d\n"          % indiv)
        #print mstheader
        # create temp file with name.. we don't need to delete because we will delete the dir later
        mstinfile = tempfile.NamedTemporaryFile(dir=tempdir, delete=False, prefix="mstin_")

        # write to temp file
        with mstinfile as inputfile:
            inputfile.write(mstheader)
            for row in genos:
                inputfile.write(row)
            inputfile.flush()

    # create iteration for output filename
    mstiter = 1
    # todo: keep track of the parameters in each iteration, to print with summary..

    # loop through each iteration file, running mstmap for it
    for root, dirs, files in os.walk(tempdir):
        for file in files:
            if file.startswith("mstin_"):

                # generate mstout file name in temp dir
                mstoutfilename = "mstout_iter_%04d.out" % mstiter
                mstoutfullpath = tempdir + "/" + mstoutfilename

                # call MSTmap
                print "now running MSTmap iteration %d" % mstiter
                # suppress the output of mstmap..
                with open(os.devnull, 'w') as FNULL:
                    subprocess.call([mstmap, mstinfile.name, mstoutfullpath], stdout=FNULL, stderr=subprocess.STDOUT)

                # increase iteration
                mstiter += 1

    print "MSTmap done..."

    # extract summary info from output files in temp dir
    summarydata = []
    outheader = ["iteration", "linkage_groups", "group_sizes", "group_bins", "\n"]
    summarydata.append(outheader)
    # todo: fix how I write tsv file..
    for root, dirs, files in os.walk(tempdir):
        for file in files:
            if file.endswith(".out"):
                # maybe this slash won't work on windows?
                fullpath = root + "/" + file
                with open(fullpath, 'r') as f:
                    # extract numbers from first lines of output
                    for i, line in enumerate(f):
                        if i == 0:
                            linkgroups = line
                            linkgroups = [x.strip() for x in linkgroups.split(":")][1]
                        if i == 2:
                            groupsize = line
                            groupsize = [x.strip() for x in groupsize.split(";")][1]
                        if i == 4:
                            groupbins = line
                            groupbins = [x.strip() for x in groupbins.split(";")][1]
                    # add values to list to print later .. newline for each iteration
                    # first create variable to append to nested list
                    nextiter = [file, linkgroups, groupsize, groupbins, "\n"]
                    summarydata.append(nextiter)

# output summary to file
    timestamp = time.strftime("%Y%m%d_%H%M%S")
    outfile = "MSTmap_summary_" + timestamp + ".tsv"
    with open(outfile, 'w') as outputfile:
        outputfile.writelines("\t".join(eachline) for eachline in summarydata)

    # cleanup temp files & dirs
    try:
        print "deleting temp directory..."
        shutil.rmtree(tempdir)  # delete temp directory
    except OSError as exc:
        if exc.errno != 2:  # code 2 - no such file or directory
            raise  # re-raise exception

    print "all done..."

if __name__ == "__main__":
    main()
