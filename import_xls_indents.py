# -*- coding: utf-8 -*-

# background_colour_index: https://xlrd.readthedocs.io/en/latest/formatting.html#palette
# indent_level and other formatting: https://xlrd.readthedocs.io/en/latest/api.html

import pandas as pd
from xlrd import open_workbook
import sys, os

def return_xls_indents(input_filename, output_filename, n_headers=2):
    """Grab the indent and color values from the first col of the sheet, add
    as columns to a dataframe. Only works with .xls, not other extensions"""

    # so I think the idea is we merge metadata with the data
    # We'll get the data now

    #open workbook using package (keeps as excel object)
    wb = open_workbook(input_filename, formatting_info=True)
    
    # print all data from all sheets
    """
    for s in wb.sheets():
        print('Sheet:', s.name)
        for row in range(s.nrows):
            values = []
            for col in range(s.ncols):
                values.append(s.cell(row,col).value)
            print(','.join(map(str, values)))
        print()
    """

    # here are some basics
    # sheet1 = wb.sheet_by_index(0)
    # print(sheet1.name)
    # print(sheet1.nrows)
    # print(sheet1.ncols)
    # remember python starts with zero
    # row_index = 0
    # col_index = 0
    # row_index = 5
    # col_index = 7
    # print(sheet1.cell(5, 7))
    # same as above
    # print sheet1.cell(row_index, col_index).value
    # print sheet1.cell(row_index, col_index).value

    # so there are lots of attributes buried in these cells.
    # This has a list starting on page 30:
    # https://github.com/python-excel/tutorial/raw/master/python-excel.pdf
    # these are XF records = styles
    # indent is a sub of alignment

    # an example that tells you all the formatting vars. uncomment to print them
    # so this is where you get the names of the parameters to extract the ones you want

    """
    xf_index = 1
    fmt = wb.xf_list[xf_index]
    print('type(fmt) is{}'.format(type(fmt)))
    print('fmt.dump(): {}'.format(fmt.dump()))
    """

    # so the xf index is separate from the cell info?
    # print(sheet1.cell(row_index, col_index).xf_index)
    # save this index number
    # xfindex = sheet1.cell(row_index, col_index).xf_index
    # somehow this index can pull information at the wb level.. unrelated to cell?? maybe this is like a dictionary?
    # or maybe it is a separate list for each cell.. who knows..
    # in any case, the indent level can be 0-15
    # print wb.xf_list[xf_index].alignment.indent_level


    # grab indent info for first column
    for s in wb.sheets():
        #print('Sheet:',s.name)
        indent_val = []
        bk_val = []
        for row in range(n_headers, s.nrows):
            for col in [0]: # range(s.ncols): (just the first col)
                xf_index = s.cell(row, col).xf_index # grab the formatting index
                indent_val.append(wb.xf_list[xf_index].alignment.indent_level)
                bk_val.append(wb.xf_list[xf_index].background.pattern_colour_index)
        # print(','.join(map(str,indent_val)))
        # print(','.join(map(str,bk_val)))

    # import the file, but as a df
    df = pd.read_excel(input_filename, header=None, index_col=None)

    # delete the first two rows of header info
    df = df.iloc[n_headers:]

    # add indent and color to DF
    df = df.assign(indent_val = indent_val)
    df = df.assign(bk_val = bk_val)

    # take df and make it csv and spit it out...
    df.to_csv(output_filename, index=False, header=False)


if __name__ == '__main__':
        # get filename from command line
        input_filename = sys.argv[1]
        output_filename = sys.argv[2]
        headers = sys.argv[3]
        # call function if file exists
        if os.path.exists(input_filename) == True:
                return_xls_indents(input_filename, output_filename, headers)
        # if file not found, exit
        else:
                print ("The input filename you specified cannot be found.")
                sys.exit()