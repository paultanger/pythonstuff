#python startup from Asa Ben-Hur

#define history file
import readline, atexit, rlcompleter,os
histfile=os.environ["HOME"] + "/python/.python_history"

#tab autocomplete
readline.parse_and_bind('tab: complete')

# try to load it
try:
    readline.read_history_file(histfile)
except IOError:
    print "can't find history file"  # It doesn't exist yet.

# save history on exit
def savehist():
    try:
        global histfile
        readline.write_history_file(histfile)
    except:
        print 'Unable to save Python command history'

#limit file size
readline.set_history_length(1000)

#register the above function
atexit.register(savehist)
del atexit

# specify path for modules (currently in my scripts directory)
import sys, os, math

sys.path.append(".")
sys.path.append(os.environ['HOME'])
path = ['/Users/paultanger/python/scripts']
path.extend(sys.path)
sys.path = path

print "startup file loaded"
