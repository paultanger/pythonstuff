# process files from Eli into hapmap files
# Paul Tanger

from datetime import datetime
import sys, os
FORMAT = '%Y%m%d%H%M%S'

def convertToHapmap(input_filename, outfilename):
    # create output filename
    outfilenamestamp = outfilename + "_" + datetime.now().strftime(FORMAT) + ".hapmap"
    # print outfilenamestamp
    # create output file
    with open(outfilenamestamp, "a") as outputfile:
        # ensure that file is closed after
        with open(input_filename, 'r') as rawfile:
            firstline = True
            for line in rawfile:
                # get header info
                if firstline:
                    firstline = False
                    header = line.strip().split('\t')
                    # create final header and write to file
                    newheader = ["rs#", "fakealleles", "chrom", "pos", "strand", "assembly", "center", "protLSID", "assayLSID", "panelLSID", "QCcode"]
                    finalheader = newheader + header[2:]
                    outputfile.write("\t".join(finalheader))
                    outputfile.write("\n")
                    continue
                # print finalheader
                # get line contents
                temp = line.strip().split('\t')
                # get marker name
                marker = temp[0].split('_')
                # print marker
                # calculate SNP position
                # print "pos is", temp[1]
                pos = int(marker[2]) - int(temp[1])
                # print pos
                # get chr
                chr = str(marker[0][3:])
                # if only one digit add padding zero
                # if len(chr) == 1:
                #     chr = "0" + chr[0:]
                # print "chr is", chr
                # create new marker name
                markername = "S" + chr + "_" + str(pos)
                # print markername
                # convert missing to N and hets to H
                # print temp[2:]
                # new calls list
                calls = []
                for SNP in temp[2:]:
                    # print SNP
                    # missing
                    if SNP == '0':
                        # print "missing"
                        calls += "N"
                    # het
                    elif len(SNP) > 1:
                        # print "het"
                        # obviously would be better if we used het coding, but we don't care about which het it is
                        calls += "R"
                    # otherwise keep the call
                    else:
                        calls += SNP
                # print calls
                # create edited line
                emptycols = ["NA"] * 6
                formatted = [markername, "A/A", chr, str(pos), "+"]
                formatted += emptycols
                formatted += calls
                # print formatted
                # write edited line to file
                outputfile.write("\t".join(formatted))
                outputfile.write("\n")
    # with the output file still open, read it and sort it
    with(open(outfilenamestamp, "r+")) as finaloutput:
        # data = finaloutput.read()
        # finaloutput.seek(0)
        finaldata = []
        firstline = True
        for line in finaloutput:
            # skip header
            if firstline:
                firstline = False
                theheader = line
                # print theheader
                continue
            # get each line
            line = line.split('\t')
            # print line
            finaldata.append(line)
        # sort the lines by chr and pos
        finaldata.sort(key=lambda s:(int(s[2]),int(s[3])))
        # print finaldata
        # clear the file contents
        finaloutput.seek(0)
        finaloutput.truncate()
        # write it
        finaloutput.write(theheader)
        for item in finaldata:
            finaloutput.write("\t".join(item))
        # finaloutput.truncate()



if __name__ == '__main__'  :
    # get filename from command line
    input_filename = sys.argv[1]
    outfilename = sys.argv[2]
    # call function if file exists
    if os.path.exists(input_filename) == True:
        convertToHapmap(input_filename, outfilename)
    # if file not found, exit
    else:
        print "The input filename you specified cannot be found."
        sys.exit()