import sys, os
from Bio.SeqIO.QualityIO import FastqGeneralIterator
def extract_reads(input_filename, barcodes):
        #create dictionary of barcodes
        barcode_dict = {}
        #ensure that file is closed after
        with open(barcodes, 'r') as barcodefile:
                # open barcode and create dictionary
                for line in barcodefile:
                        (barcode, sampleID) = line.split()
                        barcode_dict[barcode] = sampleID
        #search fastq file for matching barcodes, and dump into new files with sampleID names
        with open(input_filename, 'r') as fastqfile:
                for title, seq, qual in FastqGeneralIterator(fastqfile):
                        for barcode, sampleID in barcode_dict.iteritems():
			    if seq.startswith(barcode):
                                with open(sampleID + "_" + barcode + ".fq", "a") as outputfile:
					outputfile.write("@%s\n%s\n+\n%s\n" % (title,seq,qual))     
if __name__ == '__main__'  :
        # get filename from command line
        input_filename = sys.argv[1]
        barcodes = sys.argv[2]
        # call function if file exists
        if os.path.exists(input_filename) == True:                                   
                extract_reads(input_filename, barcodes)
        # if file not found, exit
        else:
                print "The input filename you specified cannot be found."
                sys.exit()
