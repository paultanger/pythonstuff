# -*- coding: utf-8 -*-

import pandas as pd
import sys, os

def return_xls(input_filename, output_filename, n_headers=2):
    """fixes headers and other stuff in FTFMS export files. Only works with .xls, not other extensions"""

    # otherwise python thinks a number is a char
    n_headers = int(n_headers)

    # change index_col to 0 if you want the df index to be col A from the file. Recommend not doing this because Col A is not unique
    # this reads the entire file into a variable
    # the idea here is get the proper header info together
    df = pd.read_excel(input_filename, header=None, index_col=None)

    # flatten headers into column names
    df.iloc[0:n_headers] = df.iloc[0:n_headers].fillna(method='ffill', axis=1)
    df.iloc[0:n_headers] = df.iloc[0:n_headers].fillna('')
    df.columns = df.iloc[0:n_headers][::-1].apply(lambda x: '_'.join([y for y in x if y]), axis=0)
    df = df.iloc[n_headers:]

    # rename - use a shorter header to work with the first col
    # the first thing is the name you are replacing
    df = df.rename({'Indicator / Operating Unit / Implementing Mechanism / Disaggregation':'Ind / OU / IM / Dis / Type'},axis='columns')

    # df.to_excel(output_filename, index=False)
    # pass this df to the next function which will add the indent and color coding
    return df

if __name__ == '__main__'  :
        # get filename from command line
        input_filename = sys.argv[1]
        output_filename = sys.argv[2]
        headers = sys.argv[3]
        # call function if file exists
        if os.path.exists(input_filename) == True:
                return_xls(input_filename, output_filename, headers)
        # if file not found, exit
        else:
                print ("The input filename you specified cannot be found.")
                sys.exit()


# maybe this is a better way to do this:
#
# try:
#     “””put whole function here”””
    # read sys args first
#     df.to_csv(outfile)
#     output = outfile
# except:
#     output = None
# finally:
#     return output