# -*- coding: utf-8 -*-

# background_colour_index: https://xlrd.readthedocs.io/en/latest/formatting.html#palette
# indent_level and other formatting: https://xlrd.readthedocs.io/en/latest/api.html

import pandas as pd
from xlrd import open_workbook
import sys, os

def return_xls_indents(input_filename, output_filename, n_headers=2):
    """Fixes col headings, Grab the indent and color values from the first col of the sheet, add
    as columns to a dataframe. Only works with .xls, not other extensions"""

    # otherwise python thinks a number is a char
    n_headers = int(n_headers)

    # change index_col to 0 if you want the df index to be col A from the file. Recommend not doing this because Col A is not unique
    # this reads the entire file into a variable
    # the idea here is get the proper header info together
    df = pd.read_excel(input_filename, header=None, index_col=None)

    # flatten headers into column names
    df.iloc[0:n_headers] = df.iloc[0:n_headers].fillna(method='ffill', axis=1)
    df.iloc[0:n_headers] = df.iloc[0:n_headers].fillna('')
    df.columns = df.iloc[0:n_headers][::-1].apply(lambda x: '_'.join([y for y in x if y]), axis=0)
    df = df.iloc[n_headers:]

    # rename - use a shorter header to work with the first col
    # the first thing is the name you are replacing
    df = df.rename({'Indicator / Operating Unit / Implementing Mechanism / Disaggregation':'Ind / OU / IM / Dis / Type'}, axis='columns')

    # we'll use df to combine with what we do next
    #open workbook using package (keeps as excel object)
    wb = open_workbook(input_filename, formatting_info=True)

    # grab indent and color info for first column, store in two arrays
    for s in wb.sheets():
        #print('Sheet:',s.name)
        indent_val = []
        bk_val = []
        for row in range(n_headers, s.nrows): # start after the headers
            for col in [0]: # (just the first col)
                xf_index = s.cell(row, col).xf_index # grab the formatting index
                indent_val.append(wb.xf_list[xf_index].alignment.indent_level)
                bk_val.append(wb.xf_list[xf_index].background.pattern_colour_index)


    # add indent and color to DF
    df = df.assign(indent_val = indent_val)
    df = df.assign(bk_val = bk_val)

    # take df and make it csv and spit it out...
    df.to_csv(output_filename, index=False, header=True)


if __name__ == '__main__':
        # get filename from command line
        input_filename = sys.argv[1]
        output_filename = sys.argv[2]
        headers = sys.argv[3]
        # call function if file exists
        if os.path.exists(input_filename) == True:
                return_xls_indents(input_filename, output_filename, headers)
        # if file not found, exit
        else:
                print ("The input filename you specified cannot be found.")
                sys.exit()